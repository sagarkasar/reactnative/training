import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Contact from "../Screens/Contact/Contact";
import Home from "../Screens/Home/Home";
import About from "../Screens/About/About";

const  Stack= createNativeStackNavigator();

export default function HomeStack(){
    return(
        <Stack.Navigator 
        screenOptions={
           {headerShown:false}
        }
        >
            <Stack.Screen name={'Home'} component={Home}/>
            <Stack.Screen name={'Contact'} component={Contact}/>
            <Stack.Screen name={'About'} component={About}/>
        </Stack.Navigator>
    )
}