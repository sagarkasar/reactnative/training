import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Entypo from "react-native-vector-icons/Entypo";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import HomeStack from "./HomeStack";
import ContactStack from "./ContactStack";
import AboutStack from "./AboutStack";


const Tab = createBottomTabNavigator();

function BottomTab() {
  return (
   
      <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown:false,
        tabBarActiveTintColor:'red',
        tabBarInactiveTintColor:'gray',
        tabBarShowLabel:false,
        tabBarStyle:{
          position:'absolute',
          backgroundColor: '#defcf1',
        },
        
      }}
      >
        <Tab.Screen  
          name={'HomeStack'}
          component={HomeStack}
          options={{
              tabBarIcon: ({ focused }) => (
                <Entypo
                  name='home'
                  size={30}
                  style={{ marginTop: 15}}
                />
              ),  
            }}
        />
        <Tab.Screen 
        name={'ContactStack'}
        component={ContactStack}
        options={{
              tabBarIcon: ({ focused }) => (
                <AntDesign
                  name='profile'
                  size={35}
                  style={{ marginTop: 15 }}
                />
              ),
            }}
        />
        <Tab.Screen 
        name={'AboutStack'}
        component={AboutStack}
        options={{
              tabBarIcon: ({ focused }) => (
                <MaterialCommunityIcons
                  name='account'
                  size={35}
                  style={{ marginTop: 15 }}
                />
              ),
            }}
        />
      </Tab.Navigator>
   
  );
}

export default BottomTab;