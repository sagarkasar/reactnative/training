import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Contact from "../Screens/Contact/Contact";
import About from "../Screens/About/About";
import Home from "../Screens/Home/Home";

const  Stack= createNativeStackNavigator();

export default function ContactStack(){
    return(
        <Stack.Navigator 
        screenOptions={
           {headerShown:false}
        }
        >
            <Stack.Screen name={'Contact'} component={Contact}/>
            <Stack.Screen name={'About'} component={About}/>
            <Stack.Screen name={'Home'} component={Home}/>
        </Stack.Navigator>
    )
}