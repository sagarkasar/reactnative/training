import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import { AboutStyles } from './AboutStyle';
import { HomeStyles } from '../Home/HomeStyle';


const About = ({navigation}) => {
  return (
     <View style={AboutStyles.container}>
      <Text style={AboutStyles.txt}>Hello This is About page</Text>
        <View style={HomeStyles.btnView}>
            <TouchableOpacity onPress={()=>navigation.navigate('Contact')}>
                <Text style={HomeStyles.btn}>Contact</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>navigation.navigate('Home')}>
                <Text style={HomeStyles.btn}>Home</Text>
            </TouchableOpacity>
        </View>
    </View>
  );
};

export default About;