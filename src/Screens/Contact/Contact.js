import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React from 'react';
import { ContactStyles } from './ContactStyle';
import { HomeStyles } from '../Home/HomeStyle';
import * as Animatable from 'react-native-animatable';
import About from '../About/About';

About
const Contact = ({navigation}) => {
  return (
     <View style={ContactStyles.container}>
      <Animatable.Text animation='zoomInDown' style={ContactStyles.txt}>Hello This is Contact page</Animatable.Text>
      <Animatable.View animation='zoomInDown' style={HomeStyles.btnView}>
        <TouchableOpacity onPress={()=>navigation.navigate('Home')}>
            <Text style={HomeStyles.btn}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>navigation.navigate('About')}>
            <Text style={HomeStyles.btn}>About</Text>
        </TouchableOpacity>
      </Animatable.View>
      <About />
    </View>
  );
};

export default Contact;