import React from 'react';
import { StyleSheet } from 'react-native';

export const ContactStyles = StyleSheet.create({
     container: {
        flex: 1,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
    },
    txt:{
        color: 'green',
        fontSize: 20
    }
});