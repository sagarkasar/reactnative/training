import React from 'react';
import { StyleSheet } from 'react-native';

export const HomeStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    txt:{
        color: 'green',
        fontSize: 20
    },
    btn:{
        color: 'white',
        fontSize: 16,
        borderColor: 'yellow',
        borderWidth: 1,
        padding: 10,
        backgroundColor: 'gray',
        borderRadius: 10
    },
    btnView:{ 
        flexDirection: 'row', 
        width: 200, 
        marginTop: 20
    },
    input:{
        padding: 10,
        backgroundColor: 'red',
        borderRadius: 10,
        borderWidth: 2,
        borderColor: 'green'
    },
        logo: {
        width: 305,
        height: 159,
        marginBottom: 10,
  },
});